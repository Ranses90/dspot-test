import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { BootstrapIconsModule } from 'ng-bootstrap-icons';
import { allIcons } from 'ng-bootstrap-icons/icons';
import { AvatarComponent } from './components/avatar/avatar.component';
import { FriendListItemComponent } from './components/friend-list-item/friend-list-item.component';
import { ImagesGalleryComponent } from './components/images-gallery/images-gallery.component';
import { InfoTabComponent } from './components/info-tab/info-tab.component';
import { PhotosTabComponent } from './components/photos-tab/photos-tab.component';
import { BackgroundImageDirective } from './directives/background-image.directive';
import { DetailComponent } from './components/info-tab/detail/detail.component';

@NgModule({
  declarations: [
    FriendListItemComponent,
    BackgroundImageDirective,
    InfoTabComponent,
    PhotosTabComponent,
    ImagesGalleryComponent,
    AvatarComponent,
    DetailComponent,
  ],
  imports: [CommonModule, BootstrapIconsModule.pick(allIcons)],
  providers: [],
  exports: [
    FriendListItemComponent,
    BackgroundImageDirective,
    InfoTabComponent,
    PhotosTabComponent,
    ImagesGalleryComponent,
    AvatarComponent,
  ],
})
export class SharedModule {}
