import { DOCUMENT } from '@angular/common';
import {
  Directive,
  ElementRef,
  Inject,
  Input,
  OnChanges,
  OnInit,
} from '@angular/core';

@Directive({
  selector: '[appBackgroundImage]',
})
export class BackgroundImageDirective implements OnInit, OnChanges {
  @Input() image!: string;
  @Input() altImage!: string;
  private imageAux!: HTMLImageElement;
  hasError!: boolean;
  constructor(
    private el: ElementRef<HTMLElement>,
    @Inject(DOCUMENT) private document: Document
  ) {}

  setBackground() {
    const el = this.el;
    el.nativeElement.style.backgroundSize = 'cover';
    el.nativeElement.style.backgroundPosition = 'center';
    el.nativeElement.style.backgroundImage = `url("${this.image}")`;
    el.nativeElement.style.backgroundRepeat = 'no-repeat';
    // fetch(this.image).catch(() => (el.nativeElement.style.backgroundImage = `url("${this.altImage}")`));
    if (this.imageAux) {
      this.imageAux.remove();
    }
    const img = this.document.createElement('img');
    img.style.display = 'none';
    img.onerror = () =>
      (el.nativeElement.style.backgroundImage = `url("${this.altImage}")`);
    img.src = this.image;
    this.imageAux = img;
    el.nativeElement.prepend(img);
  }

  ngOnInit() {}

  ngOnChanges() {
    this.setBackground();
  }
}
