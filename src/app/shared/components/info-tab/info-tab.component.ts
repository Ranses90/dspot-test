import { Component, Input, OnInit } from '@angular/core';
import { FriendModel } from 'src/app/models/friend';

@Component({
  selector: 'app-info-tab',
  templateUrl: './info-tab.component.html',
  styleUrls: ['./info-tab.component.scss'],
})
export class InfoTabComponent implements OnInit {
  @Input() friend!: FriendModel;

  constructor() {}

  ngOnInit(): void {}
}
