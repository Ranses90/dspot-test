import { Component, Input, OnInit } from '@angular/core';
import { FriendModel } from 'src/app/models/friend';

@Component({
  selector: 'app-photos-tab',
  templateUrl: './photos-tab.component.html',
  styleUrls: ['./photos-tab.component.scss'],
})
export class PhotosTabComponent implements OnInit {
  @Input() friend!: FriendModel;

  constructor() {}

  ngOnInit(): void {}
}
