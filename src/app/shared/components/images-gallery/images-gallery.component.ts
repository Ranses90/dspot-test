import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-images-gallery',
  templateUrl: './images-gallery.component.html',
  styleUrls: ['./images-gallery.component.scss'],
})
export class ImagesGalleryComponent implements OnInit {
  selected?: string;

  @Input() photos: string[] = [];

  constructor() {}

  ngOnInit() {}

  onSelectPhoto(src: string) {
    this.selected = src;
  }

  onClose() {
    this.selected = undefined;
  }
}
