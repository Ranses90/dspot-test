import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-avatar',
  templateUrl: './avatar.component.html',
  styleUrls: ['./avatar.component.scss'],
})
export class AvatarComponent implements OnInit {
  @Input() image: string = '';
  @Input() width: number = 0;
  @Input() height: number = 0;
  @Input() hasMargin = true;

  constructor() {}

  ngOnInit(): void {}
}
