import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FriendModel } from '../../../models/friend';

@Component({
  selector: 'app-friend-list-item',
  templateUrl: './friend-list-item.component.html',
  styleUrls: ['./friend-list-item.component.scss'],
})
export class FriendListItemComponent implements OnInit {
  @Input() friend: FriendModel = new FriendModel();

  constructor(private router: Router) {}

  ngOnInit(): void {}

  goToDetails(id: number) {
    this.router.navigateByUrl('/friends/' + id);
  }
}
