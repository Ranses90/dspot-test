import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { FriendDetailsComponent } from '../../../views/friends/friend-details/friend-details.component';
import { SharedModule } from '../../shared.module';

import { FriendListItemComponent } from './friend-list-item.component';

describe('FriendListItemComponent', () => {
  let component: FriendListItemComponent;
  let fixture: ComponentFixture<FriendListItemComponent>;
  let router: Router;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [FriendListItemComponent],
      imports: [
        RouterTestingModule.withRoutes([
          { path: 'friends/1', component: FriendDetailsComponent },
        ]),
        SharedModule,
      ],
    }).compileComponents();
    router = TestBed.inject(Router);
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FriendListItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  test('should create', () => {
    expect(component).toBeTruthy();
  });

  test('Details button click should be called', () => {
    fixture.detectChanges();
    jest.spyOn(fixture.componentInstance, 'goToDetails'); //method attached to the click.
    let btn = fixture.elementRef.nativeElement.querySelector('.btn');
    btn.click();
    fixture.detectChanges();
    expect(fixture.componentInstance.goToDetails).toHaveBeenCalled();
  });

  test('should go to friend details view', () => {
    const navigateSpy = jest.spyOn(router, 'navigateByUrl');
    component.goToDetails(1);
    expect(navigateSpy).toHaveBeenCalledWith('/friends/1');
  });
});
