import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'friends', pathMatch: 'full' },
  {
    path: 'friends',
    loadChildren: () =>
      import('./views/friends/friends.module').then((m) => m.FriendsModule),
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
