import { Friend } from '../interfaces/friend';

export class FriendModel {
  id: number;
  img: string;
  first_name: string;
  last_name: string;
  status: string;
  available: boolean;
  address_1: string;
  bio: string;
  city: string;
  phone: string;
  photos: string[];
  state: string;
  statuses: string[];
  zipcode: string;

  constructor(
    friend: Friend = {
      id: 0,
      img: '',
      first_name: '',
      last_name: '',
      status: '',
      available: false,
      address_1: '',
      bio: '',
      city: '',
      phone: '',
      photos: [],
      state: '',
      statuses: [],
      zipcode: '',
    }
  ) {
    this.id = friend.id;
    this.img = friend.img;
    this.first_name = friend.first_name;
    this.last_name = friend.last_name;
    this.status = friend.status;
    this.available = friend.available;
    this.address_1 = friend.address_1;
    this.bio = friend.bio;
    this.city = friend.city;
    this.phone = friend.phone;
    this.photos = friend.photos;
    this.state = friend.state;
    this.statuses = friend.statuses;
    this.zipcode = friend.zipcode;
  }
}
