import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map, Observable } from 'rxjs';
import { API_URL } from '../../environments/environment';
import { Friend } from '../interfaces/friend';
import { FriendModel } from '../models/friend';

@Injectable({
  providedIn: 'root',
})
export class FriendsService {
  baseUrl = API_URL + '/friends';
  constructor(private http: HttpClient) {}

  getAll(
    skip: number,
    take: number,
    filters: string
  ): Observable<FriendModel[]> {
    return this.http
      .get<Friend[]>(this.baseUrl, {
        params: {
          skip,
          take,
          filters,
        },
      })
      .pipe(map((data) => data.map((t: Friend) => new FriendModel(t))));
  }

  getById(): Observable<FriendModel> {
    return this.http
      .get<Friend>(this.baseUrl + `/id`)
      .pipe(map((data) => new FriendModel(data)));
  }
}
