import { HttpClientModule } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';
import { FriendModel } from '../models/friend';

import { FriendsService } from './friends.service';

let friend = {
  address_1: '5190 Center Court Drive',
  available: true,
  bio: "I'm very choosy. I'm also very suspicious, very irrational and I have a very short temper. I'm also extremely jealous and slow to forgive. Just so you know.",
  city: 'Spring',
  first_name: 'Steph',
  id: 6,
  img: 'https://s3.amazonaws.com/uifaces/faces/twitter/walterstephanie/128.jpg',
  last_name: 'Walters',
  phone: '(820) 289-1818',
  photos: [
    'https://flic.kr/p/mxHVJu',
    'https://flic.kr/p/nCJyXN',
    'https://flic.kr/p/mxwwsv',
  ],
  state: 'TX',
  statuses: [
    'Developing something amazing',
    'This could be interesting....',
    'Man, life is so good',
  ],
  zipcode: '77370',
};

describe('FriendsService', () => {
  let service: FriendsService;

  beforeEach(() => {
    TestBed.configureTestingModule({ imports: [HttpClientModule] });
    service = TestBed.inject(FriendsService);
  });

  test('should be created', () => {
    expect(service).toBeTruthy();
  });

  test('should fetch correct friend', () => {
    service.getById().subscribe((data: FriendModel) => {
      expect(data).toEqual(friend);
    });
  });

  test('should fetch 6 friends', () => {
    service.getAll(1, 10, '').subscribe((data: FriendModel[]) => {
      expect(data.length).toBe(6);
    });
  });
});
