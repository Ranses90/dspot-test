import { CommonModule } from '@angular/common';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { CacheInterceptor } from '../interceptors/cache.interceptor';
import { AuthModule } from './auth/auth.module';
import { CacheMapService } from './cache/cache-map.service';

@NgModule({
  declarations: [],
  imports: [CommonModule, AuthModule, HttpClientModule],
  providers: [
    CacheMapService,
    { provide: Cache, useClass: CacheMapService },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: CacheInterceptor,
      multi: true,
    },
  ],
})
export class CoreModule {}
