export interface Friend {
  id: number;
  img: string;
  first_name: string;
  last_name: string;
  status: string;
  available: boolean;
  address_1: string;
  bio: string;
  city: string;
  phone: string;
  photos: string[];
  state: string;
  statuses: string[];
  zipcode: string;
}
