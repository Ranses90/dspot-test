import { HttpClientModule } from '@angular/common/http';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { StoreModule } from '@ngrx/store';
import { BootstrapIconsModule } from 'ng-bootstrap-icons';
import { NgxSpinnerModule } from 'ngx-spinner';
import { of } from 'rxjs';
import { Friend } from 'src/app/interfaces/friend';
import { SharedModule } from '../../../shared/shared.module';

import { FriendDetailsComponent } from './friend-details.component';

describe('FriendDetailsComponent', () => {
  let component: FriendDetailsComponent;
  let fixture: ComponentFixture<FriendDetailsComponent>;

  let friend: Friend = {
    available: false,
    first_name: 'Jeremy',
    id: 1,
    img: 'https://s3.amazonaws.com/uifaces/faces/twitter/csswizardry/128.jpg',
    last_name: 'Davis',
    status: 'At work...',
    address_1: '',
    bio: '',
    city: '',
    phone: '',
    photos: [],
    state: '',
    statuses: [],
    zipcode: '',
  };

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [FriendDetailsComponent],
      imports: [
        HttpClientModule,
        NgbModule,
        SharedModule,
        NgxSpinnerModule,
        BootstrapIconsModule,
        RouterTestingModule,
        StoreModule.forFeature('friends', {}),
        StoreModule.forRoot({}),
      ],
      providers: [],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FriendDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  test('should create', () => {
    expect(component).toBeTruthy();
  });

  test('info tab works', () => {
    component.activeTab = 1;
    component.friend$ = of(friend);
    fixture.detectChanges();
    const compEl = fixture.elementRef.nativeElement;
    const infoTab = compEl.querySelector('app-info-tab');
    expect(infoTab).toBeTruthy();
  });

  test('photos tab works', () => {
    component.activeTab = 2;
    component.friend$ = of(friend);
    fixture.detectChanges();
    const compEl = fixture.elementRef.nativeElement;
    const photosTab = compEl.querySelector('app-photos-tab');
    expect(photosTab).toBeTruthy();
  });
});
