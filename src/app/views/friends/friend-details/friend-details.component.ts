import { Location } from '@angular/common';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subject, takeUntil } from 'rxjs';
import { FriendsStoreService } from '../../../store/services/friend-store.service';

@Component({
  selector: 'app-friend-details',
  templateUrl: './friend-details.component.html',
  styleUrls: ['./friend-details.component.scss'],
})
export class FriendDetailsComponent implements OnInit, OnDestroy {
  destroy$: Subject<boolean> = new Subject<boolean>();
  friendId: number = 0;
  activeTab = 1;

  friend$ = this.friendService.selectedFriend$;

  constructor(
    private route: ActivatedRoute,
    private friendService: FriendsStoreService,
    private location: Location
  ) {}

  ngOnInit(): void {
    // Subscription to url id friend change, it is working until the component it is destroyed
    this.route.params
      .pipe(takeUntil(this.destroy$))
      .subscribe((routeParams) => {
        this.friendId = +routeParams['id'];
        this.friendService.loadFriend(6);
      });
  }

  ngOnDestroy(): void {
    // Set value true in order to unsubscribe from routeParams
    this.destroy$.next(true);
    this.destroy$.unsubscribe();
  }

  goBack() {
    this.location.back();
  }
}
