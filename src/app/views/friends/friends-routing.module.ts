import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FriendDetailsComponent } from './friend-details/friend-details.component';
import { FriendsComponent } from './friends.component';

const routes: Routes = [
  { path: '', component: FriendsComponent },
  { path: ':id', component: FriendDetailsComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FriendsRoutingModule {}
