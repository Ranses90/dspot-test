import { HttpClientModule } from '@angular/common/http';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { StoreModule } from '@ngrx/store';
import { NgxSpinnerModule, NgxSpinnerService } from 'ngx-spinner';
import { of } from 'rxjs';
import { SharedModule } from '../../shared/shared.module';

import { Friend } from 'src/app/interfaces/friend';
import { FriendsComponent } from './friends.component';

describe('FriendsComponent', () => {
  let component: FriendsComponent;
  let fixture: ComponentFixture<FriendsComponent>;

  let friends: Friend[] = [
    {
      available: false,
      first_name: 'Jeremy',
      id: 1,
      img: 'https://s3.amazonaws.com/uifaces/faces/twitter/csswizardry/128.jpg',
      last_name: 'Davis',
      status: 'At work...',
      address_1: '',
      bio: '',
      city: '',
      phone: '',
      photos: [],
      state: '',
      statuses: [],
      zipcode: '',
    },
    {
      available: true,
      first_name: 'Vlad',
      id: 2,
      img: 'https://s3.amazonaws.com/uifaces/faces/twitter/vladarbatov/128.jpg',
      last_name: 'Baratovich',
      status: 'Hangout out by the pool',
      address_1: '',
      bio: '',
      city: '',
      phone: '',
      photos: [],
      state: '',
      statuses: [],
      zipcode: '',
    },
    {
      available: true,
      first_name: 'Reese',
      id: 3,
      img: 'https://s3.amazonaws.com/uifaces/faces/twitter/rssems/128.jpg',
      last_name: 'Samsonite',
      status: 'At NG-conf!',
      address_1: '',
      bio: '',
      city: '',
      phone: '',
      photos: [],
      state: '',
      statuses: [],
      zipcode: '',
    },
  ];

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [FriendsComponent],
      imports: [
        HttpClientModule,
        NgxSpinnerModule,
        SharedModule,
        RouterTestingModule,
        StoreModule.forFeature('friends', {}),
        StoreModule.forRoot({}),
      ],
      providers: [NgxSpinnerService],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FriendsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  test('should create', () => {
    expect(component).toBeTruthy();
  });

  test('ngx spinner should be shown', () => {
    fixture.detectChanges();
    const compEl = fixture.elementRef.nativeElement;
    const spinnerEl = compEl.querySelector('ngx-spinner');
    expect(spinnerEl).toBeTruthy();
  });

  test('friends list should have one child for each friend', () => {
    component.friendsList$ = of(friends);
    fixture.detectChanges();
    const compEl = fixture.elementRef.nativeElement;
    const friendsListEl: HTMLUListElement = compEl.querySelector('.friends');
    expect(friendsListEl.childElementCount).toBe(3);
  });
});
