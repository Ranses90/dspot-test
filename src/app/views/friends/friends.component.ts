import { Component, OnInit } from '@angular/core';
import { FriendModel } from 'src/app/models/friend';
import { FriendsStoreService } from '../../store/services/friend-store.service';

@Component({
  selector: 'app-friends',
  templateUrl: './friends.component.html',
  styleUrls: ['./friends.component.scss'],
})
export class FriendsComponent implements OnInit {
  friendsList$ = this.friendService.allFriends$;

  constructor(private friendService: FriendsStoreService) {}

  ngOnInit(): void {
    this.friendService.loadFriends();
  }

  // TrackBy function to allow specify the changes separately
  trackByFn(index: number, item: FriendModel) {
    return item.id;
  }
}
