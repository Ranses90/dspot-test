import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { BootstrapIconsModule } from 'ng-bootstrap-icons';
import { allIcons } from 'ng-bootstrap-icons/icons';
import { NgxSpinnerModule } from 'ngx-spinner';
import { FriendsService } from 'src/app/services/friends.service';
import { SharedModule } from 'src/app/shared/shared.module';
import { FriendDetailsComponent } from './friend-details/friend-details.component';
import { FriendsRoutingModule } from './friends-routing.module';
import { FriendsComponent } from './friends.component';

@NgModule({
  declarations: [FriendsComponent, FriendDetailsComponent],
  imports: [
    FriendsRoutingModule,
    CommonModule,
    BootstrapIconsModule.pick(allIcons),
    NgbModule,
    SharedModule,
    NgxSpinnerModule,
  ],
  providers: [FriendsService],
  bootstrap: [FriendsComponent],
})
export class FriendsModule {}
